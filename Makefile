CC = gcc
CFLAGS = -Wall -Werror -g
stkmachine: main.o stack.o shell.o bin
	${CC} ${CFLAGS} -o bin/stkmachine main.o stack.o shell.o	
	@echo Built target 'stkmachine'
stktest: test/stktest.o stack.o bin
	${CC} ${CFLAGS} -o bin/stktest test/stktest.o stack.o
	@echo Built target 'stktest'
shtest: test/shtest.o shell.o bin
	${CC} ${CFLAGS} -o bin/shtest test/shtest.o shell.o
	@echo Built target 'shtest'
main.o: main.c
	${CC} ${CFLAGS} -c main.c
stack.o: stack.c
	${CC} ${CFLAGS} -c stack.c
shell.o: shell.c
	${CC} ${CFLAGS} -c shell.c
stktest.o: test/stktest.c
	${CC} ${CFLAGS} -c -o test/stktest.o test/stktest.c
shtest.o: test/shtest.c
	${CC} ${CFLAGS} -c -o test/stktest.o test/stktest.c
bin:
	mkdir -p bin
clean:
	rm -f main.o stack.o shell.o test/stktest.o test/shtest.o
	rm -rf bin 

An interactive stack machine with support for basic arithmetic and push/pop instructions.

#Build:
`make stkmachine`

#Tests:
##Stack:
`make stktest`
##Interactive Shell:
`make shtest`

#Instruction list:
##push
prompts and subsequently places user inputted value on the stack.
##pop
removes the topmost value on the stack.
##add
removes topmost value on the stack and adds it to the next topmost element.
##sub
removes topmost value on the stack, and subtracts it from the next topmost element.
##mul
removes topmost value on the stack, and multiplies the next element by this value.
##div
removes topmost value on the stack, and divides the next element by this value.
##print
prints the stack as is done at each prompt - here for debug purposes.
##exit
self-explanatory.

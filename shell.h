#ifndef SHELL_H
#define SHELL_H

//basic struct produced by command interpeter
//contains basic instruction info for stack
struct shell_cmd{
	char *command;
	int operand;
};
//simple command prompt
//ret: input string
struct shell_cmd *shellprompt();

#endif

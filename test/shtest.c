#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../shell.h"

int main()
{
	while(1) {
		struct shell_cmd *instr = shellprompt();
		printf("Instruction: %s\nOperand: %d\n", instr->command, instr->operand);
		free(instr);
	}
}

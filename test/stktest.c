#include <stdio.h>
#include <stdlib.h>

#include "../stack.h"

int main(int argc, char *argv[])
{
	struct stkcell *head = NULL;

	int stacklim = (argc == 2) ? atoi(argv[1]) : 8;

	printf("Stack test\n");
	int i;
	for (i = 0; i < stacklim; ++i) {
		head = stkpush(i, head);
		printf("head value: %d\n", head->val);
	}
	stkprint(head);
	while (head != NULL)
		head = stkpop(head);
	stkprint(head);
	return 0;
}

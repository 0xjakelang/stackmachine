#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "stack.h"

struct stkcell *stkpush(int value, struct stkcell *head)
{
	//allocate new stack element and move current head
	struct stkcell *new = calloc(1, sizeof(struct stkcell));

	new->val = value;
	new->next = head;

	return new;
}

struct stkcell *stkpop(struct stkcell *head)
{
	assert(head != NULL);

	struct stkcell *tmp = head;

	head = head->next;
	free(tmp);

	return head;
}

void stkprint(struct stkcell *head)
{
	struct stkcell *tmp = head;
	printf("stack dump :");
	while (tmp != NULL) {
		printf(" %d", tmp->val);
		tmp = tmp->next;
	}
	putchar('\n');
}

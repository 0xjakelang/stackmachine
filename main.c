#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stack.h"
#include "shell.h"

#define DEBUG 1
const char *commands[] =  {"push",
			    "pop",
			    "add",
			    "sub",
			    "mul",
			    "div",
			    "print",
			    "exit"};

int main(int argc, char *argv[])
{
	struct stkcell *stkhead = NULL;
	struct shell_cmd *inputcmd;
	int i, tmp;
	int opcode, opr;
	int exitflag = 0;

	printf("Tower v1.0\n");

	while (!exitflag) {
		#ifdef DEBUG
		stkprint(stkhead);
		#endif

		opcode = -1;
		opr = 0;
		tmp = 0;
		inputcmd = shellprompt();
		
		for (i = 0; i < 8; ++i) {
			if (!strcmp(inputcmd->command, commands[i])) {
				opcode = i;	
				break;
			}
		}
		opr = inputcmd->operand;
		free(inputcmd);

		/*TODO: eliminate redundancies*/
		switch (opcode) {
			case 0:
				stkhead = stkpush(opr, stkhead);
				break;
			case 1:
				stkhead = stkpop(stkhead);
				break;
			case 2:
				if (stkhead->next == NULL) {
					printf("Not enough operands\n");
				} else {
					tmp = stkhead->val;
					stkhead = stkpop(stkhead);
					stkhead->val += tmp;
				}
				break;
			case 3:
				if (stkhead->next == NULL) {
					printf("Not enough operands\n");
				} else {
					tmp = stkhead->val;
					stkhead = stkpop(stkhead);
					stkhead->val -= tmp;
				}
				break;
			case 4:
				if (stkhead->next == NULL) {
					printf("Not enough operands\n");
				} else {
					tmp = stkhead->val;
					stkhead = stkpop(stkhead);
					stkhead->val *= tmp;
				}
				break;
			case 5:
				if (stkhead->next == NULL) {
					printf("Not enough operands\n");
				} else {
					tmp = stkhead->val;
					stkhead = stkpop(stkhead);
					stkhead->val /= tmp;
				}
				break;
			case 6:
				stkprint(stkhead);
				break;
			case 7:
				exitflag = 1;
				printf("Exitting...\n");
				break;
			default:
				printf("Invalid operand\n");
				break;
		}
	}
}

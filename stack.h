#ifndef STACK_H
#define STACK_H

#include <stdint.h>

//element of a stack, in our case being a linked-list structure
//pointer singleton to head of stack must be kept
struct stkcell {
	int val;
	struct stkcell *next;
};

//allocates a new stack element on top of head
//args: signed int value, current head of list
//ret: new head
struct stkcell *stkpush(int value, struct stkcell *head);

//frees stack element at head, moves head to next head of list
//args: current head of list
//ret: new head
struct stkcell *stkpop(struct stkcell *head);

//dumps all values on stack
//args: current head of list
void stkprint(struct stkcell *head);
#endif

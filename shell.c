#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "shell.h"

struct shell_cmd *shellprompt()
{
	struct shell_cmd *op = calloc(1, sizeof(struct shell_cmd));
	char input[6];
	int opr;
/*
 *	printf("enter instruction >");
 *	scanf("%15s", input);
 *	op->command = calloc(strlen(input), sizeof(char));
 *	strcpy(op->command, input);
 *
 *	printf("enter operand >");
 *	scanf("%15s", input);
 *	op->operand = (!strcmp(op->command, "push")) ? atoi(input) : 0;
 */
	
	printf("> ");
	scanf("%5s", input);

	op->command = calloc(strlen(input), sizeof(char));
	strcpy(op->command, input);
	
	if (!strcmp(input, "push")) {
		printf("operand > ");
		op->operand = (scanf("%d", &opr)) ? opr : 0;
	} else
		op->operand = 0;

	return op;
}
